package com.touristhotel.www.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;


import org.w3c.dom.Text;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEmployeeFragment extends Fragment {

    private Button btnCancel;
    private Button btnSubmit;
    private EditText txtFirstname;
    private EditText txtLastname;
    private EditText txtUsername;
    private EditText txtPassword;
    private Spinner spinQuestion;
    private EditText txtAnswer;
    private RadioGroup radType;

    public AddEmployeeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnCancel=getActivity().findViewById(R.id.btnCancel);
        btnSubmit=getActivity().findViewById(R.id.btnSubmit);
        txtFirstname=getActivity().findViewById(R.id.txtFirstname);
        txtLastname=getActivity().findViewById(R.id.txtLastname);
        txtUsername=getActivity().findViewById(R.id.txtUsername);
        txtPassword=getActivity().findViewById(R.id.txtPass);
        spinQuestion=getActivity().findViewById(R.id.spinQuestion);
        txtAnswer=getActivity().findViewById(R.id.txtAnswer);
        radType=getActivity().findViewById(R.id.radType);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }
    public void register(){
        String firstname=txtFirstname.getText().toString();
        String lastname=txtLastname.getText().toString();
        String username=txtUsername.getText().toString();
        String password=txtPassword.getText().toString();
        String question=spinQuestion.getSelectedItem().toString();
        String answer=txtAnswer.getText().toString();
        int rad=radType.getCheckedRadioButtonId();
        String type="";
        if(rad==R.id.radBarman)
            type="barman";
        else if(rad==R.id.radCheif)
            type="cheif";
        if(TextUtils.isEmpty(firstname) || TextUtils.isEmpty(lastname) || TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(answer)){
            Toast.makeText(AddEmployeeFragment.this.getContext(),"Fill all required fields and try again!",Toast.LENGTH_LONG).show();
        }
        RequestBody body=new FormBody.Builder()
                .add("username",username)
                .add("firstname",firstname)
                .add("lastname",lastname)
                .add("password",password)
                .add("question",question)
                .add("answer",answer)
                .add("type",type)
                .build();
        LoginActivity.post(LoginActivity.url + "/tourist/register_employee.php", body, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("FAILURE",e.getMessage());
                AddEmployeeFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AddEmployeeFragment.this.getContext(),"Connection to the server failed",Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String result=response.body().string();
                Log.d("SUCCESS",result);
                AddEmployeeFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                     if(result==null || result.contains("false")){
                         Toast.makeText(AddEmployeeFragment.this.getContext(),"Sorry, an error occured",Toast.LENGTH_LONG).show();
                     }else if(result.contains("duplicate")){
                         Toast.makeText(AddEmployeeFragment.this.getContext(),"Sorry, your username or email is already used by another user!",Toast.LENGTH_LONG).show();
                     }
                     else if(result.contains("true")){
                         Toast.makeText(AddEmployeeFragment.this.getContext(),"Registration is succesfull",Toast.LENGTH_LONG).show();
                         Intent intent=new Intent(AddEmployeeFragment.this.getContext(),LoginActivity.class);
                         startActivity(intent);
                         AddEmployeeFragment.this.getActivity().finish();
                     }
                    }
                });
            }
        });
    }
    public void cancel(){
        txtFirstname.setText("");
        txtLastname.setText("");
        txtUsername.setText("");
        txtAnswer.setText("");
        txtPassword.setText("");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_employee, container, false);
    }

}
