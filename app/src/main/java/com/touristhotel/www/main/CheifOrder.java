package com.touristhotel.www.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by geek on 5/10/2018.
 */

public class CheifOrder {
    String username;
    String item;
    String status;
    String date;
    String mid;
    String id;
    String tableNo;
    static ArrayList<CheifOrder> orders=new ArrayList<>(100);
    public CheifOrder(){

    }
    public static void loadOrders( final Activity activity){
        orders=new ArrayList<>(100);
        RequestBody body=new FormBody.Builder()
                .add("orders","order")
                .build();
        LoginActivity.post(LoginActivity.url + "/tourist/cheif.php", body, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("Cheif Response: ",e.getMessage());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity,"Unable to connect to server!",Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String result=response.body().string();
                Log.d("Cheif",result);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(result!=null && result.contains(";")){
                            String[] results=result.split(";");
                            int counter=0;
                            for(String r:results){
                                if(r!=null && r.contains(",")){
                                    counter++;
                                    String[] data=r.split(",");
                                    String id=data[0];
                                    String mid=data[1];
                                    String username=data[2];
                                    String orderDate=data[3];
                                    String status=data[4];
                                    String item=data[5];
                                    String table=data[6];
                                    final CheifOrder order=new CheifOrder();
                                    order.item=item;
                                    order.date=orderDate;
                                    order.username=username;
                                    order.status=status;
                                    order.id=id;
                                    order.mid=mid;
                                    order.tableNo=table;
                                    LayoutInflater inflater=activity.getLayoutInflater();
                                    LinearLayout layout=(LinearLayout)inflater.inflate(R.layout.cheif_order,null);
                                    TextView txtItem=layout.findViewById(R.id.txtItem);
                                    TextView txtUser=layout.findViewById(R.id.txtUser);
                                    TextView txtDate=layout.findViewById(R.id.txtDate);
                                    TextView txtTable=layout.findViewById(R.id.txtTable);
                                    txtUser.setText(""+counter+". Ordered By: "+txtUser);
                                    txtItem.setText("Ordered Item: "+item);
                                    txtDate.setText("Order Date: "+orderDate);
                                    txtTable.setText("Table Number: "+table);
                                    Button btnServe=layout.findViewById(R.id.btnServe);
                                    btnServe.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            serve(activity,order);
                                        }
                                    });
                                    LinearLayout parent=activity.findViewById(R.id.cheifParent);
                                    parent.addView(layout);
                                }
                            }
                        }
                    }
                });
            }
        });
    }
    public static void serve(final Activity activity, CheifOrder order){
        RequestBody body=new FormBody.Builder()
                .add("serve","serve")
                .add("id",order.id)
                .add("mid",order.mid)
                .add("username",order.username)
                .build();
        LoginActivity.post(LoginActivity.url + "/tourist/cheif.php", body, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("SERVE",e.getMessage());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity,"Connection to server failed! please try again",Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String result=response.body().string();
                Log.d("SERVE: ",result);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(result!=null && result.contains("true")){
                            AlertDialog.Builder builder=new AlertDialog.Builder(activity);
                            builder.setTitle("Success");
                            builder.setMessage("You are now responsible to serve this user as the other chiefs will no longer access this request.");
                            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }
                        else if(result!=null && result.contains("false")){
                            AlertDialog.Builder builder=new AlertDialog.Builder(activity);
                            builder.setTitle("Failed");
                            builder.setMessage("You are not selected to serve to this item, may be other users have it already, please close this application and reopen it to refresh the content!");

                            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }
                    }
                });
            }
        });
    }
}
