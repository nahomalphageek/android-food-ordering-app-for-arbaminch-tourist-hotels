package com.touristhotel.www.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {
    private Button btnLogin;
    private Button btnSignup;
    private Button btnReset;
    static OkHttpClient client;
    static String url="http://10.0.3.2";
    EditText txtUsername;
    EditText txtPassword;
    public static String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin=(Button)findViewById(R.id.login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login(view);
            }
        });
        Button btnContact=findViewById(R.id.btnContact);
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,ContactActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnSignup=(Button)findViewById(R.id.signup);
        txtUsername=findViewById(R.id.txtUser);
        txtPassword=findViewById(R.id.txtPass);
        btnReset=findViewById(R.id.btnReset);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp(view);
            }
        });
        CookieManager cookieManager=new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        client=new OkHttpClient();
        client=new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("RESET","CLICKED");
                resetPassword();
            }
        });
    }
    static String usernameReset="";
    public AlertDialog getProcess(){
        AlertDialog dialog=new AlertDialog.Builder(this)
                .setTitle("Operation is in Progress")
                .setMessage("Please wait a momment...")
                .create();
        return dialog;
    }
    public void resetPassword(){

        final AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Reset Password");
        final EditText txtUser=new EditText(LoginActivity.this);
        txtUser.setHint("username");
        txtUser.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(txtUser);
        builder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                usernameReset=txtUser.getText().toString();
                if(TextUtils.isEmpty(usernameReset))
                    Toast.makeText(LoginActivity.this,"Please enter your username",Toast.LENGTH_LONG).show();
                else {

                    requestQuestion(usernameReset);
                }

            }
        });
        builder.show();
    }
    public void requestQuestion(String username){
        final AlertDialog progress=getProcess();
        progress.show();
        RequestBody body=new FormBody.Builder()
                .add("username",username)
                .add("question","q")
                .add("mode","get")
                .build();
        LoginActivity.post(LoginActivity.url + "/tourist/reset.php", body, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("RESET",e.getMessage());
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.cancel();
                        Toast.makeText(LoginActivity.this,"Error, connection failed!",Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                    final String result=response.body().string();
                    Log.d("RESET RESPONSE: ",result);
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress.cancel();
                            if(result!=null && result.contains(";;")){
                                try {
                                    String question = result.split(";;")[0];
                                    String answer = result.split(";;")[1];
                                    final AlertDialog.Builder step2 = new AlertDialog.Builder(LoginActivity.this);
                                    step2.setTitle("Answer Security Question");
                                    final LinearLayout layout = new LinearLayout(LoginActivity.this);
                                    layout.setOrientation(LinearLayout.VERTICAL);
                                    final TextView txtQuestion = new TextView(step2.getContext());
                                    final EditText txtSecAnswer = new EditText(step2.getContext());
                                    final EditText txtPass = new EditText(step2.getContext());
                                    txtQuestion.setText(question);
                                    txtSecAnswer.setHint("Security Answer");
                                    txtPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                    txtPass.setHint("New Password");
                                    layout.addView(txtQuestion);
                                    layout.addView(txtSecAnswer);
                                    layout.addView(txtPass);
                                    step2.setView(layout);
                                    step2.setPositiveButton("RESET", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            String qn = txtQuestion.getText().toString();
                                            String an = txtSecAnswer.getText().toString();
                                            String pass = txtPass.getText().toString();
                                            if (TextUtils.isEmpty(an)) {
                                                Toast.makeText(LoginActivity.this, "Please answer the security question to continue!", Toast.LENGTH_LONG).show();

                                            } else if (TextUtils.isEmpty(pass)) {
                                                Toast.makeText(LoginActivity.this, "Please answer the security question to continue!", Toast.LENGTH_LONG).show();
                                            } else {
                                                RequestBody body = new FormBody.Builder()
                                                        .add("username", usernameReset)
                                                        .add("password", pass)
                                                        .add("question", qn)
                                                        .add("answer", an)
                                                        .add("mode", "set")
                                                        .build();
                                                LoginActivity.post(LoginActivity.url + "/tourist/reset.php", body, new Callback() {
                                                    @Override
                                                    public void onFailure(Call call, IOException e) {
                                                        Log.d("RESET-SET: ", e.getMessage());
                                                        LoginActivity.this.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(LoginActivity.this, "Connection to server failed", Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void onResponse(Call call, Response response) throws IOException {
                                                        final String result = response.body().string();
                                                        Log.d("RESET-SET: ", result);
                                                        LoginActivity.this.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if (result.contains("true")) {
                                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                                                    dialog.setTitle("Success");
                                                                    dialog.setMessage("Your password is reset successfully, login using your new password");
                                                                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialogInterface, int i) {

                                                                        }
                                                                    });
                                                                    dialog.show();
                                                                } else if (result.contains("wrong_answer")) {
                                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                                                    dialog.setTitle("Reset Failed");
                                                                    dialog.setMessage("Password reset failed due to wrong answer!");
                                                                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialogInterface, int i) {

                                                                        }
                                                                    });
                                                                    dialog.show();
                                                                } else {
                                                                    AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                                                    dialog.setTitle("Reset Failed");
                                                                    dialog.setMessage("Password reset failed due to unknow error!");
                                                                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialogInterface, int i) {

                                                                        }
                                                                    });
                                                                    dialog.show();
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    step2.show();
                                }catch(ArrayIndexOutOfBoundsException ex){
                                    Toast.makeText(LoginActivity.this,"Error: "+ex.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                            else{
                                AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);
                                builder.setTitle("Reset Failed");
                                builder.setMessage("Either You didn't set security questions or you're not registered on our network");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                                builder.show();
                            }
                        }
                    });
            }
        });
    }
    public void Login(View v){
        final AlertDialog progress=getProcess();
        progress.show();
        String username=txtUsername.getText().toString();
        String password=txtPassword.getText().toString();
        LoginActivity.username=username;
        if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
            Toast.makeText(this,"Both Fields are required!",Toast.LENGTH_LONG).show();
        }else{
            RequestBody body=new FormBody.Builder()
                    .add("username",username)
                    .add("password",password)
                    .build();
            LoginActivity.post(LoginActivity.url + "/tourist/login.php", body, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            vibrate();
                            progress.cancel();
                            AlertDialog dialog =new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle("Connection Failed")
                                    .setIcon(R.drawable.ic_report_black_24dp)
                                    .setMessage("Connection to tourist server failed, connect to our wifi network and try again!")
                                    .create();
                            dialog.show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String r=response.body().string();
                    Log.d("TOURIST","Response:"+r);
                    if(r!=null && !TextUtils.isEmpty(r)){
                        if(r.equals("true")){
                            LoginActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    vibrate();
                                    progress.cancel();
                                    Intent intent=new Intent(LoginActivity.this,UserActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                        }
                        else if(r.equals("admin")){
                            LoginActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.cancel();
                                    Intent intent=new Intent(LoginActivity.this,AdminActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        }
                        else if(r.equalsIgnoreCase("cheif")){

                            Intent intent=new Intent(LoginActivity.this,ChefActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else if(r.equals("bartender")){

                        }
                        else{

                            LoginActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.cancel();
                                    vibrate();
                                    Toast.makeText(LoginActivity.this,"Wrong username or password",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                }
            }
        });
    }
    }
    public void vibrate(){
            Vibrator v=(Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            if(v.hasVibrator()){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
                }
            }else{
                v.vibrate(500);
            }
    }
    public void signUp(View v){
        Intent intent=new Intent(this,RegisterActivity.class);
        startActivity(intent);
        finish();
    }
    static Call post(String url, RequestBody body, Callback callback){
        Request request=new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call=client.newCall(request);
        call.enqueue(callback);
        return call;
    }
}
